resource "aws_iam_role" "cw_role" {
  count = var.cloudwatch_role == true ? 1 : 0
  name  = "CloudWatch-CrossAccountSharingRole"
  path  = "/"
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/CloudWatchReadOnlyAccess",
    "arn:aws:iam::aws:policy/CloudWatchAutomaticDashboardsAccess"
  ]
  assume_role_policy = data.aws_iam_policy_document.cloudwatchassumepolicy[0].json
}

data "aws_iam_policy_document" "cloudwatchassumepolicy" {
  count = var.cloudwatch_role == true ? 1 : 0
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "AWS"
      identifiers = [format("arn:aws:iam::%s:root", var.security)]
    }
  }
}
