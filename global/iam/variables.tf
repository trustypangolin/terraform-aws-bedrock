# Mandatory Variables
variable "unique_prefix" {
  type        = string
  description = "prefix used to be part of resource name"
}

variable "env" {
  type        = string
  description = "prefix used to be part of resource name"
}

variable "security" {
  description = "Security Account, Used to accept invites from delegated services"
  type        = string
}

variable "central" {
  type        = string
  description = "AWS Account that contains the Central Lambda Functions"
}

# Optional Variables
variable "crossaccountrole" {
  type        = string
  description = "Cross Account Role"
  default     = "bedrock-central-lambda-remote"
}

variable "grafana_id" {
  type        = string
  description = "This is your Grafana Cloud identifier and is used for security purposes."
  default     = null
}

variable "grafana_role_name" {
  type        = string
  description = "Customize the name of the IAM role used by Grafana for the CloudWatch Integration."
  default     = "GrafanaLabsCloudWatchIntegration"
}

# For Integration with existing AWS accounts, we need to set these
variable "password_policy" {
  type        = bool
  description = "Apply Default Password Policy"
  default     = true
}

variable "public_s3_block" {
  type        = bool
  description = "Apply S3 Public Access Block Settings"
  default     = true
}

variable "lambda_crossaccount_role" {
  type        = bool
  description = "Create Lambda Cross Account Role"
  default     = true
}

variable "aws_support_role" {
  type        = bool
  description = "Create AWS Support Role"
  default     = true
}

variable "cloudwatch_role" {
  type        = bool
  description = "Create CloudWatch Cross Account Role"
  default     = true
}

variable "config_service_role" {
  type        = bool
  description = "Create Config Service Role"
  default     = true
}

data "aws_caller_identity" "current" {}
