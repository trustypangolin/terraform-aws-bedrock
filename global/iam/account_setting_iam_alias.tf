resource "aws_iam_account_alias" "env" {
  account_alias = format("%s-%s", var.unique_prefix, var.env)
}

data "aws_iam_account_alias" "env" {
  depends_on = [
    aws_iam_account_alias.env,
  ]
}
