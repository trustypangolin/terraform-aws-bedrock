resource "aws_iam_service_linked_role" "config" {
  count            = var.config_service_role == true ? 1 : 0
  aws_service_name = "config.amazonaws.com"
  description      = "Allows Config to call AWS services and collect resource configurations on your behalf."
}
