# Mandatory Variables
variable "unique_prefix" {
  type        = string
  description = "prefix used to be part of resource name"
}

variable "base_region" {
  type        = string
  description = "AWS region to operate in. Defaults to ap-southeast-2 (Sydney)."
  default     = "ap-southeast-2"
}

variable "security" {
  description = "Security Account, Used to accept invites from delegated services"
  type        = string
}

data "aws_caller_identity" "current" {}
data "aws_region" "current" {}
