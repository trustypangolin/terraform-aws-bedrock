variable "vpc" {
  type = object(
    {
      vpc_id   = string,
      cidr     = string,
      public   = object({ subnet_cidrs = list(string), subnet_ids = list(string), routetable_ids = list(string) }),
      private  = object({ subnet_cidrs = list(string), subnet_ids = list(string), routetable_ids = list(string) }),
      isolated = object({ subnet_cidrs = list(string), subnet_ids = list(string), routetable_ids = list(string) }),
    }
  )
}

variable "instance_type" {
  type    = string
  default = "m5a.large"
}

variable "instance_type_spot" {
  type    = string
  default = "m5a.large"
}

variable "enable_spot" {
  type    = bool
  default = false
}

variable "key_name" {
  type = string
}

variable "security_group" {
  type = list(string)
}

variable "desired_capacity" {
  type    = number
  default = 3
}

variable "min_size" {
  type    = number
  default = 3
}

variable "max_size" {
  type    = number
  default = 8
}

variable "iam_profile" {
  type = string
}

variable "tag" {
  type = map(string)
  default = {
    "Backup" : "False",
    "Name" : "2022-ecs-host",
    "bedrock-prod-patching" : "true"
  }
}

variable "dimensions" {
  type = map(string)
}

variable "env" {
  type = string
}
