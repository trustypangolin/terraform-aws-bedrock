data "aws_ami" "aws_ecs" {
  owners      = ["amazon"]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-ecs-hvm-2.0.*-x86_64-ebs"]
  }
}

resource "aws_key_pair" "ecs" {
  key_name   = format("ecs-key-%s", var.env)
  public_key = var.key_name
}

resource "aws_launch_template" "ecs_2022" {
  name                   = format("%s-ecs-2022", var.env)
  image_id               = data.aws_ami.aws_ecs.id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.ecs.key_name
  user_data              = base64encode(templatefile("${path.module}/userdata/userdata.sh", { ecs-cluster = var.env }))
  ebs_optimized          = true
  vpc_security_group_ids = var.security_group
  iam_instance_profile {
    name = var.iam_profile
  }

  monitoring {
    enabled = true
  }

  placement {
    partition_number = 0
    tenancy          = "default"
  }

  block_device_mappings {
    device_name = data.aws_ami.aws_ecs.root_device_name
    ebs {
      delete_on_termination = "true"
      iops                  = 3000
      snapshot_id           = data.aws_ami.aws_ecs.root_snapshot_id
      throughput            = 125
      volume_size           = 50
      volume_type           = "gp3"
    }
  }
}

resource "aws_autoscaling_group" "ecs_2022" {
  name                = format("%s_ecs_2022", var.env)
  vpc_zone_identifier = var.vpc.private.subnet_ids
  max_size            = var.max_size
  min_size            = var.min_size

  dynamic "tag" {
    for_each = var.tag
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  enabled_metrics = [
    "GroupAndWarmPoolDesiredCapacity",
    "GroupAndWarmPoolTotalCapacity",
    "GroupDesiredCapacity",
    "GroupInServiceCapacity",
    "GroupInServiceInstances",
    "GroupMaxSize",
    "GroupMinSize",
    "GroupPendingCapacity",
    "GroupPendingInstances",
    "GroupStandbyCapacity",
    "GroupStandbyInstances",
    "GroupTerminatingCapacity",
    "GroupTerminatingInstances",
    "GroupTotalCapacity",
    "GroupTotalInstances",
    "WarmPoolDesiredCapacity",
    "WarmPoolMinSize",
    "WarmPoolPendingCapacity",
    "WarmPoolTerminatingCapacity",
    "WarmPoolTotalCapacity",
    "WarmPoolWarmedCapacity",
  ]

  capacity_rebalance = var.enable_spot
  dynamic "launch_template" {
    for_each = var.enable_spot ? [] : [1]
    content {
      id      = aws_launch_template.ecs_2022.id
      version = "$Latest"
    }
  }

  dynamic "mixed_instances_policy" {
    for_each = var.enable_spot ? [1] : []
    content {
      instances_distribution {
        on_demand_allocation_strategy            = "prioritized"
        on_demand_base_capacity                  = 0
        on_demand_percentage_above_base_capacity = 0
        spot_allocation_strategy                 = "capacity-optimized"
        spot_instance_pools                      = 0
      }
      launch_template {
        launch_template_specification {
          launch_template_id   = aws_launch_template.ecs_2022.id
          launch_template_name = aws_launch_template.ecs_2022.name
          version              = "$Latest"
        }

        override {
          instance_type     = var.instance_type_spot
          weighted_capacity = "2"
        }
        override {
          instance_type     = var.instance_type
          weighted_capacity = "1"
        }
      }
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_policy" "ecs_2022_up" {
  name                   = format("%s-ec2_scale_up_policy", var.env)
  policy_type            = "SimpleScaling"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 60
  autoscaling_group_name = aws_autoscaling_group.ecs_2022.name
}

resource "aws_autoscaling_policy" "ecs_2022_down" {
  name                   = format("%s-ec2_scale_down_policy", var.env)
  policy_type            = "SimpleScaling"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 60
  autoscaling_group_name = aws_autoscaling_group.ecs_2022.name
}

resource "aws_autoscaling_policy" "ecs_2022_health" {
  name            = format("%s_TargetGroupHealth", var.env)
  policy_type     = "StepScaling"
  adjustment_type = "ChangeInCapacity"
  step_adjustment {
    metric_interval_upper_bound = "-1"
    scaling_adjustment          = 2
  }
  step_adjustment {
    metric_interval_lower_bound = "-1"
    metric_interval_upper_bound = "0"
    scaling_adjustment          = 1
  }
  autoscaling_group_name = aws_autoscaling_group.ecs_2022.name
}

resource "aws_cloudwatch_metric_alarm" "cpu-high" {
  alarm_name          = format("%s-ScaleAlarmHigh-ecs", var.env)
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 3
  datapoints_to_alarm = 3
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = 60
  statistic           = "Average"
  threshold           = 60
  alarm_description   = "Scale-up if CPUUtilization GreaterThanThreshold 60% for 60 seconds 3 times."
  alarm_actions = [
    aws_autoscaling_policy.ecs_2022_up.arn
  ]
  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.ecs_2022.name
  }
}

resource "aws_cloudwatch_metric_alarm" "cpu-low" {
  alarm_name          = format("%s-ScaleAlarmLow-ecs", var.env)
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = 3
  datapoints_to_alarm = 3
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = 300
  statistic           = "Average"
  threshold           = 30
  alarm_description   = "Scale-down if CPUUtilization LessThanThreshold 30% for 300 seconds 3 times."
  alarm_actions = [
    aws_autoscaling_policy.ecs_2022_down.arn
  ]
  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.ecs_2022.name
  }
}

resource "aws_cloudwatch_metric_alarm" "healthcheck" {
  alarm_name          = format("%s - ALB Healthy Host Count Alarm", var.env)
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = 5
  datapoints_to_alarm = 5
  metric_name         = "HealthyHostCount"
  namespace           = "AWS/ApplicationELB"
  period              = 60
  statistic           = "Minimum"
  threshold           = 2
  alarm_description   = format("%s - ALB Cluster Healthy Host Count Alarm", var.env)
  alarm_actions = [
    aws_autoscaling_policy.ecs_2022_health.arn
    # ,"arn:aws:sns:us-east-1:393224382150:Production_ECS_Alerts"
  ]
  dimensions = var.dimensions
}
