variable "zone_id" {
  type        = string
  description = "Route53 Zone ID - eg Z08541321C6F8H7PXA834"
}

variable "zone_name" {
  type        = string
  description = "Route53 Zone name - eg somedomain.com"
}

variable "zone_record" {
  type        = string
  description = "the ACM record appended to  the zone_name - eg someval(.zone.name)"
  default     = "*"
}
