resource "aws_cloudwatch_log_group" "api" {
  name              = format("/aws/lambda/%s", aws_lambda_function.api.function_name)
  retention_in_days = 3
}

resource "aws_iam_role" "api" {
  name                 = local.name
  max_session_duration = 43200
  managed_policy_arns  = ["arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"]
  assume_role_policy   = data.aws_iam_policy_document.assume_lambda.json

  inline_policy {
    name   = "AccessS3Books"
    policy = data.aws_iam_policy_document.lambda_s3.json
  }

  inline_policy {
    name   = "AllowKMS"
    policy = data.aws_iam_policy_document.lambda_kms.json
  }
}

resource "aws_lambda_function" "api" {
  function_name    = local.name
  description      = var.description
  handler          = var.handler
  runtime          = "nodejs16.x"
  memory_size      = var.memory_size
  timeout          = var.timeout
  role             = aws_iam_role.api.arn
  filename         = var.archive_path
  source_code_hash = filebase64sha256(var.archive_path)

  environment {
    variables = {
      "S3Location" = format("%s-book", var.env)
    }
  }

  vpc_config {
    security_group_ids = [
      var.sg,
    ]
    subnet_ids = var.vpc.private.subnet_ids.*
  }
}
