data "aws_iam_policy_document" "assume_lambda" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com"
      ]
    }
    effect = "Allow"
  }
}

data "aws_iam_policy_document" "lambda_s3" {
  statement {
    sid = "s3bucketaccess"
    actions = [
      "s3:*",
    ]
    resources = [
      "*",
    ]
    effect = "Allow"
  }
}

data "aws_iam_policy_document" "lambda_kms" {
  statement {
    sid = "lambdakms"
    actions = [
      "kms:*"
    ]
    resources = [
      "arn:aws:kms:*:*:alias/aws/lambda",
    ]
    effect = "Allow"
  }
}

# -- Function execution
#--------------------------------------------------------------------------------------------------
# data "aws_lambda_invocation" "executer" {
#   # depends_on    = [module.modules_for_all_accounts]
#   function_name = aws_lambda_function.delete_default_vpcs_function.function_name
#   input = <<JSON
# {
#   "RequestType": "Create",
#   "ResponseURL": "http://pre-signed-S3-url-for-response",
#   "StackId": "arn:aws:cloudformation:ap-southeast-2:123456789012:stack/MyStack/guid",
#   "RequestId": "unique id for this create request",
#   "ResourceType": "Custom::TestResource",
#   "LogicalResourceId": "MyTestResource",
#   "ResourceProperties": {
#     "StackName": "MyStack",
#     "List": [
#       "1",
#       "2",
#       "3"
#     ]
#   }
# }
# JSON
# }
