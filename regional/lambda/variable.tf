variable "env" {
  type        = string
  description = "Resource Prefix"
}

variable "vpc" {
  type = object(
    {
      vpc_id   = string,
      cidr     = string,
      public   = object({ subnet_cidrs = list(string), subnet_ids = list(string), routetable_ids = list(string) }),
      private  = object({ subnet_cidrs = list(string), subnet_ids = list(string), routetable_ids = list(string) }),
      isolated = object({ subnet_cidrs = list(string), subnet_ids = list(string), routetable_ids = list(string) }),
    }
  )
}

variable "sg" {
  type        = string
  description = "Lambda Security Group"
}

variable "archive_path" {
  type        = string
  description = "Path to the lambda's deployment archive"
}

variable "name" {
  type        = string
  description = "The lambda function's name"
}

variable "description" {
  type    = string
  default = ""
}

variable "handler" {
  type    = string
  default = "lambda.handler"
}

variable "memory_size" {
  type    = number
  default = 256
}

variable "timeout" {
  type    = number
  default = 10
}
