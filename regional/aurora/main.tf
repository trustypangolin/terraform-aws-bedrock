locals {
  port             = var.engine == "aurora-postgresql" ? 5432 : 3306
  is_serverless    = var.engine_mode == "serverless"
  backtrack_window = (var.engine == "aurora-mysql" || var.engine == "aurora") && var.engine_mode != "serverless" ? var.backtrack_window : 0
}

resource "aws_db_subnet_group" "db-sgrp" {
  name       = format("%s_db_sgrp", var.env)
  subnet_ids = var.vpc.private.subnet_ids
}

module "security_group" {
  source = "gitlab.com/douughlabs/douugh-terraform-modules/aws//regional/security_group"
  name   = format("%s_db", var.env)
  env    = var.env
  vpc_id = var.vpc.vpc_id
}

resource "aws_security_group_rule" "ecs_to_db" {
  description              = format("From %s ECS Cluster", var.env)
  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "TCP"
  source_security_group_id = var.source_security_group_id
  security_group_id        = module.security_group.security_group_id
}

################################################################################
# RDS Aurora Module - PostgreSQL Serverless V2
################################################################################
data "aws_rds_engine_version" "postgresql" {
  engine  = "aurora-postgresql"
  version = "13.6"
}

resource "aws_rds_cluster" "this" {
  count = var.create_cluster ? 1 : 0

  cluster_identifier = format("%s-db", var.env)

  # Notes:
  # iam_roles has been removed from this resource and instead will be used with aws_rds_cluster_role_association below to avoid conflicts per docs

  # Default Null Values
  # global_cluster_identifier      = var.global_cluster_identifier
  # enable_global_write_forwarding = var.enable_global_write_forwarding
  # replication_source_identifier = var.replication_source_identifier
  # source_region = var.source_region

  engine                      = var.engine
  engine_mode                 = var.engine_mode
  engine_version              = local.is_serverless ? null : var.engine_version
  allow_major_version_upgrade = var.allow_major_version_upgrade
  enable_http_endpoint        = var.enable_http_endpoint
  kms_key_id                  = var.kms_key_id
  database_name               = var.is_primary_cluster ? var.db_name : null
  master_username             = var.is_primary_cluster ? var.db_username : null
  master_password             = var.is_primary_cluster ? var.db_password : null
  # final_snapshot_identifier           = "${var.final_snapshot_identifier_prefix}-${var.name}-${element(concat(random_id.snapshot_identifier.*.hex, [""]), 0)}"
  skip_final_snapshot                 = var.skip_final_snapshot
  deletion_protection                 = var.deletion_protection
  backup_retention_period             = var.backup_retention_period
  preferred_backup_window             = local.is_serverless ? null : var.preferred_backup_window
  preferred_maintenance_window        = local.is_serverless ? null : var.preferred_maintenance_window
  port                                = local.port
  db_subnet_group_name                = aws_db_subnet_group.db-sgrp.id
  vpc_security_group_ids              = [module.security_group.security_group_id]
  snapshot_identifier                 = var.snapshot_identifier
  storage_encrypted                   = var.storage_encrypted
  apply_immediately                   = var.apply_immediately
  db_cluster_parameter_group_name     = var.db_cluster_parameter_group_name
  db_instance_parameter_group_name    = var.allow_major_version_upgrade ? var.db_cluster_db_instance_parameter_group_name : null
  iam_database_authentication_enabled = var.iam_database_authentication_enabled
  backtrack_window                    = local.backtrack_window
  copy_tags_to_snapshot               = var.copy_tags_to_snapshot
  enabled_cloudwatch_logs_exports     = var.enabled_cloudwatch_logs_exports

  # timeouts {
  #   create = lookup(var.cluster_timeouts, "create", null)
  #   update = lookup(var.cluster_timeouts, "update", null)
  #   delete = lookup(var.cluster_timeouts, "delete", null)
  # }

  dynamic "scaling_configuration" {
    for_each = length(keys(var.scaling_configuration)) == 0 || !local.is_serverless ? [] : [var.scaling_configuration]

    content {
      auto_pause               = lookup(scaling_configuration.value, "auto_pause", null)
      max_capacity             = lookup(scaling_configuration.value, "max_capacity", null)
      min_capacity             = lookup(scaling_configuration.value, "min_capacity", null)
      seconds_until_auto_pause = lookup(scaling_configuration.value, "seconds_until_auto_pause", null)
      timeout_action           = lookup(scaling_configuration.value, "timeout_action", null)
    }
  }

  dynamic "serverlessv2_scaling_configuration" {
    for_each = length(keys(var.serverlessv2_scaling_configuration)) == 0 || local.is_serverless ? [] : [var.serverlessv2_scaling_configuration]

    content {
      max_capacity = serverlessv2_scaling_configuration.value.max_capacity
      min_capacity = serverlessv2_scaling_configuration.value.min_capacity
    }
  }
  # dynamic "s3_import" {
  #   for_each = var.s3_import != null && !local.is_serverless ? [var.s3_import] : []
  #   content {
  #     source_engine         = "mysql"
  #     source_engine_version = s3_import.value.source_engine_version
  #     bucket_name           = s3_import.value.bucket_name
  #     bucket_prefix         = lookup(s3_import.value, "bucket_prefix", null)
  #     ingestion_role        = s3_import.value.ingestion_role
  #   }
  # }

  # dynamic "restore_to_point_in_time" {
  #   for_each = length(keys(var.restore_to_point_in_time)) == 0 ? [] : [var.restore_to_point_in_time]

  #   content {
  #     source_cluster_identifier  = restore_to_point_in_time.value.source_cluster_identifier
  #     restore_type               = lookup(restore_to_point_in_time.value, "restore_type", null)
  #     use_latest_restorable_time = lookup(restore_to_point_in_time.value, "use_latest_restorable_time", null)
  #     restore_to_time            = lookup(restore_to_point_in_time.value, "restore_to_time", null)
  #   }
  # }

  lifecycle {
    ignore_changes = [
      # See https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/rds_cluster#replication_source_identifier
      # Since this is used either in read-replica clusters or global clusters, this should be acceptable to specify
      replication_source_identifier,
      # See docs here https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/rds_global_cluster#new-global-cluster-from-existing-db-cluster
      global_cluster_identifier,
    ]
  }
}

resource "aws_rds_cluster_instance" "this" {
  for_each = var.create_cluster && !local.is_serverless ? var.instances : {}

  # Notes:
  # Do not set preferred_backup_window - its set at the cluster level and will error if provided here

  identifier         = var.instances_use_identifier_prefix ? null : lookup(each.value, "identifier", "${var.env}-db-${each.key}")
  identifier_prefix  = var.instances_use_identifier_prefix ? lookup(each.value, "identifier_prefix", "${var.env}-db-${each.key}-") : null
  cluster_identifier = try(aws_rds_cluster.this[0].id, "")
  engine             = var.engine
  engine_version     = var.engine_version
  instance_class     = lookup(each.value, "instance_class", var.db_instance_class)
  # publicly_accessible                   = lookup(each.value, "publicly_accessible", var.publicly_accessible)
  # db_subnet_group_name                  = local.db_subnet_group_name
  # db_parameter_group_name               = lookup(each.value, "db_parameter_group_name", var.db_parameter_group_name)
  # apply_immediately                     = lookup(each.value, "apply_immediately", var.apply_immediately)
  # monitoring_role_arn                   = local.rds_enhanced_monitoring_arn
  # monitoring_interval                   = lookup(each.value, "monitoring_interval", var.monitoring_interval)
  # promotion_tier                        = lookup(each.value, "promotion_tier", null)
  # availability_zone                     = lookup(each.value, "availability_zone", null)
  # preferred_maintenance_window          = lookup(each.value, "preferred_maintenance_window", var.preferred_maintenance_window)
  # auto_minor_version_upgrade            = lookup(each.value, "auto_minor_version_upgrade", var.auto_minor_version_upgrade)
  # performance_insights_enabled          = lookup(each.value, "performance_insights_enabled", var.performance_insights_enabled)
  # performance_insights_kms_key_id       = lookup(each.value, "performance_insights_kms_key_id", var.performance_insights_kms_key_id)
  # performance_insights_retention_period = lookup(each.value, "performance_insights_retention_period", var.performance_insights_retention_period)
  copy_tags_to_snapshot = lookup(each.value, "copy_tags_to_snapshot", var.copy_tags_to_snapshot)
  # ca_cert_identifier                    = var.ca_cert_identifier

  # timeouts {
  #   create = lookup(var.instance_timeouts, "create", null)
  #   update = lookup(var.instance_timeouts, "update", null)
  #   delete = lookup(var.instance_timeouts, "delete", null)
  # }

}
