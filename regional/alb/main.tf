# Default ALB implementation that can be used connect ECS instances to it
resource "aws_lb" "alb" {
  name            = var.alb_name
  subnets         = var.public_subnet_ids
  security_groups = [module.security_group.security_group_id]
}

# EC2 Target Group
resource "aws_lb_target_group" "default" {
  name                 = format("%s-default", var.alb_name)
  port                 = 80
  protocol             = "HTTP"
  vpc_id               = var.vpc_id
  deregistration_delay = var.deregistration_delay

  health_check {
    path     = var.health_check_path
    protocol = "HTTP"
    interval = var.health_check_interval
    timeout  = var.health_check_timeout
  }
  depends_on = [aws_lb.alb]
}

# EC2 Tools Target Group
resource "aws_lb_target_group" "tools" {
  name                 = format("%s-tools", var.alb_name)
  port                 = 80
  protocol             = "HTTP"
  vpc_id               = var.vpc_id
  deregistration_delay = var.deregistration_delay

  health_check {
    path     = var.health_check_path_fargate
    protocol = "HTTP"
    interval = var.health_check_interval
    timeout  = var.health_check_timeout
  }
  depends_on = [aws_lb.alb]
}

# IP based/Fargate Target Group
resource "aws_lb_target_group" "fargate" {
  name                 = format("%s-default-fargate", var.alb_name)
  port                 = 80
  protocol             = "HTTP"
  vpc_id               = var.vpc_id
  deregistration_delay = var.deregistration_delay
  target_type          = "ip"

  health_check {
    path     = var.health_check_path_fargate
    protocol = "HTTP"
  }
  depends_on = [aws_lb.alb]
}

# EC2 Redirection
resource "aws_lb_listener" "https" {
  load_balancer_arn = aws_lb.alb.id
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = var.ssl_policy
  certificate_arn   = var.acm_cert
  default_action {
    target_group_arn = aws_lb_target_group.default.id
    type             = "forward"
  }
}

# Fargate Redirection
resource "aws_lb_listener" "https_fargate" {
  load_balancer_arn = aws_lb.alb.id
  port              = "8443"
  protocol          = "HTTPS"
  ssl_policy        = var.ssl_policy
  certificate_arn   = var.acm_cert
  default_action {
    target_group_arn = aws_lb_target_group.fargate.id
    type             = "forward"
  }
}

# EC2 Tools Redirection
resource "aws_lb_listener" "https_tools" {
  load_balancer_arn = aws_lb.alb.id
  port              = "9443"
  protocol          = "HTTPS"
  ssl_policy        = var.ssl_policy
  certificate_arn   = var.acm_cert
  default_action {
    target_group_arn = aws_lb_target_group.tools.id
    type             = "forward"
  }
}

module "security_group" {
  source           = "gitlab.com/douughlabs/douugh-terraform-modules/aws//regional/security_group"
  name             = format("%s_alb", var.env)
  env              = var.env
  vpc_id           = var.vpc_id
  include_outbound = false
}

resource "aws_security_group_rule" "https_from_anywhere" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "TCP"
  cidr_blocks       = [var.allow_cidr_block]
  security_group_id = module.security_group.security_group_id
}

resource "aws_security_group_rule" "outbound_internet_access" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = module.security_group.security_group_id
}
