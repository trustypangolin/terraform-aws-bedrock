variable "acm_cert" {
  type        = string
  description = "ACM Cert ARN"
}

variable "alb_name" {
  type        = string
  default     = "default"
  description = "The name of the loadbalancer"
}

variable "env" {
  type        = string
  description = "The name of the environment"
}

variable "public_subnet_ids" {
  type        = list(string)
  description = "List of public subnet ids to place the loadbalancer in"
}

variable "vpc_id" {
  type        = string
  description = "The VPC id"
}

variable "deregistration_delay" {
  type        = number
  default     = 300
  description = "The default deregistration delay"
}

variable "health_check_path" {
  type        = string
  default     = "/healthcheck/"
  description = "The default health check path"
}

variable "health_check_path_fargate" {
  type        = string
  default     = "/"
  description = "The default health check path"
}

variable "allow_cidr_block" {
  type        = string
  default     = "0.0.0.0/0"
  description = "Specify cird block that is allowd to acces the LoadBalancer"
}

variable "ssl_policy" {
  type        = string
  description = "SSL Policy"
  default     = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
}

variable "health_check_interval" {
  type    = number
  default = 30
}

variable "health_check_timeout" {
  type    = number
  default = 5
}
