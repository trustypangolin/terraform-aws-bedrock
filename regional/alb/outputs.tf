output "alb_security_group_id" {
  value = module.security_group.security_group_id
}

output "default_alb_target_group" {
  value = aws_lb_target_group.default.arn
}

output "fargate_alb_target_group" {
  value = aws_lb_target_group.fargate.arn
}

output "tools_alb_target_group" {
  value = aws_lb_target_group.tools.arn
}

output "default_alb_target_group_suffix" {
  value = aws_lb_target_group.default.arn_suffix
}

output "default_alb_arn_suffix" {
  value = aws_lb.alb.arn_suffix
}

output "listener" {
  value = aws_lb_listener.https.id
}

output "alb_dns_name" {
  value = aws_lb.alb.dns_name
}

output "alb_zone_id" {
  value = aws_lb.alb.zone_id
}
