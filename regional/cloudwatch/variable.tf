variable "loggroups" {
  type        = list(string)
  description = "Log Group List"
  default     = ["default"]
}

variable "retention" {
  type    = number
  default = 3
}
