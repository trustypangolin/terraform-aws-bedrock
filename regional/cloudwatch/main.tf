resource "aws_cloudwatch_log_group" "logs" {
  for_each          = toset(var.loggroups)
  name              = each.key
  retention_in_days = var.retention
}
